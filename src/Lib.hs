module Lib where

import Data.List(sortBy)
import Data.Time.Clock(UTCTime)
import Data.Function(on)

data Status = Available | Unavailable deriving (Eq, Show)
data Person = Person { name :: String, status :: Status } deriving (Show)
data History = History { date :: UTCTime , employees :: [Person] }
data Group = Group { name :: String, employees :: [Person], history :: [History] }

instance Eq Person where
  (==)  Person {name = name1} Person {name = name2} = name1 == name2

zipHist :: History -> [(UTCTime, Person)]
zipHist History { date , employees } = [(date, employee) | employee <- employees]

available :: Person -> Bool
available Person { status } = status == Available

prevSelections :: [History] -> [Person]
prevSelections [] = []
prevSelections histories = map snd sorted
    where zipped = concatMap zipHist histories
          sorted = sortBy (compare  `on` fst) zipped

drawEmployees :: [Person] -> [History] -> Int -> [Person]
drawEmployees [] _ _ = error "Can't draw from list of empty employees"
drawEmployees employees [] n = take n [employee | employee <- employees, available employee]
drawEmployees employees history n = take n available_employees
    where past_selections = prevSelections history
          isUnselected e = available e && notElem e past_selections
          unselected = [employee | employee <- employees, isUnselected employee]
          available_employees = unselected ++ past_selections
