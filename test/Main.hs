module Main(main) where

import Test.HUnit
import Data.Time.Clock
import Data.Time.Calendar.OrdinalDate

import Lib(zipHist, History(..))

emptyHist = History { date = UTCTime (fromOrdinalDate 2021 20) (secondsToDiffTime 0), employees = []}
testZipEmptyHist = TestCase $ assertEqual "Empty histoy is zipped into empty list" [] (zipHist emptyHist)

main :: IO Counts 
main = runTestTT $ TestList [testZipEmptyHist]
